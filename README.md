# Todo App #

## Solution for a challenge from [Devchallenges.io](http://devchallenges.io). ##


## [Demo](https://keen-goldwasser-0d8aa9.netlify.app/) | [Solution](https://devchallenges.io/solutions/IdsHe41KV0S3JRBIYtYI) | [Challenge](https://devchallenges.io/challenges/hH6PbOHBdPm6otzw2De5) ##

## Table of Contents

- [Overview](#overview)
- [Built With](#built-with)
- [Features](#features)
- [Contact](#contact)

## Overview

![screenshot](https://devchallenges.io/_next/image?url=https%3A%2F%2Ffirebasestorage.googleapis.com%2Fv0%2Fb%2Fdevchallenges-1234.appspot.com%2Fo%2FchallengesDesigns%252FtodoThumbnail.png%3Falt%3Dmedia%26token%3D67a62272-6021-49de-ab19-cda0344d643c&w=1920&q=75)

This application/site was created as a submission to a [DevChallenges](https://devchallenges.io/challenges) challenge.
The [challenge](https://devchallenges.io/challenges/hH6PbOHBdPm6otzw2De5) was to build an application to complete the given user stories.

## Built With

- [ReactJS](https://reactjs.org/)
- [TypeScript](https://www.typescriptlang.org/)
- [Styled Components](https://styled-components.com/)

## Features

This project features a responsive page showcasing a Todo app, including mobile and desktop views. Built with ReactJS, TypeScript and Styled Components.

## Contact

You can contact me [here](https://linktr.ee/rodrigodev).
