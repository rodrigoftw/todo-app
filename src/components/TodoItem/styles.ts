import styled from 'styled-components';

interface TodoItemProps {
  completed: boolean;
}

export const Container = styled.div`
  height: 2rem;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-start;

  input {
    cursor: pointer;
  }
`;

export const TodoTitle = styled.span<TodoItemProps>`
  margin-left: 0.6rem;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 22px;

  color: var(--item-text-title);
  text-decoration: ${props => props.completed ? 'line-through' : 'none'};
`;

export const DeleteTodoButton = styled.button`
  height: 2rem;
  width: 2rem;
  border: 0;
  background: transparent;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: auto;

  span {
    color: var(--gray2);
    font-size: 1.5rem;
    margin: auto;
  }
`;
