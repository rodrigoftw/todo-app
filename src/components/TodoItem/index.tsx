import { useHeader } from "../../hooks/useHeader";
import { useTodos } from "../../hooks/useTodos";
import { Container, TodoTitle, DeleteTodoButton } from "./styles";

interface TodoItemProps {
  todo: {
    id: string;
    title: string;
    completed: boolean;
  }
}

export function TodoItem({ todo }: TodoItemProps) {
  const { getSelectedHeaderItem } = useHeader();
  const { updateTodoStatus, deleteTodo } = useTodos();

  const handleChangeCheckbox = (id: string) => {
    updateTodoStatus(id);
  };

  const handleTodoRemoval = (id: string) => {
    deleteTodo(id);
  };

  return (
    <Container>
      <input
        type="checkbox"
        name="checkbox"
        id={todo.id}
        checked={todo.completed}
        onChange={() => handleChangeCheckbox(todo.id)}
      />
      <TodoTitle completed={todo.completed}>{todo.title}</TodoTitle>
      {getSelectedHeaderItem() === "#completed" &&
        <DeleteTodoButton onClick={() => handleTodoRemoval(todo.id)}>
          <span className="material-icons">delete_outline</span>
        </DeleteTodoButton>
      }
    </Container>
  );
}