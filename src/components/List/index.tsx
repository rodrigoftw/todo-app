import { useState } from "react";
import { useHeader } from "../../hooks/useHeader";
import { useTodos } from "../../hooks/useTodos";

import { TodoItem } from "../TodoItem";

import {
  Container,
  CreateTodoContainer,
  TodoItemContainer,
  AddButton,
  DeleteButton
} from "./styles";

export function List() {
  const { getSelectedHeaderItem } = useHeader();
  const {
    todos,
    getActiveTodos,
    getCompletedTodos,
    createTodo,
    deleteCompletedTodos
  } = useTodos();
  const [todoValue, setTodoValue] = useState('');

  const handleTodoInput = (event: any) => {
    setTodoValue(event.target.value);
  }

  const handleNewTodo = () => {
    if (todoValue !== '') {
      createTodo(todoValue);
    } else {
      alert('Todo must have a title. Please try again.');
    }
    setTodoValue('');
  }

  return (
    <Container>
      {
        (getSelectedHeaderItem() === "#all" ||
          getSelectedHeaderItem() === "#active") &&
        <CreateTodoContainer>
          <input
            type="text"
            placeholder="add details"
            value={todoValue}
            onChange={handleTodoInput}
          />
          <AddButton onClick={handleNewTodo}>Add</AddButton>
        </CreateTodoContainer>
      }
      {
        getSelectedHeaderItem() === "#all" && todos.map((todo) => (
          <TodoItemContainer key={todo.id}>
            <TodoItem todo={todo} />
          </TodoItemContainer>
        ))
      }

      {
        getSelectedHeaderItem() === "#active" && getActiveTodos().map((todo) => (
          <TodoItemContainer key={todo.id}>
            <TodoItem todo={todo} />
          </TodoItemContainer>
        ))
      }

      {
        getSelectedHeaderItem() === "#completed" && getCompletedTodos().map((todo) => (
          <TodoItemContainer isCompleted key={todo.id}>
            <TodoItem todo={todo} />
          </TodoItemContainer>
        ))
      }

      {(getSelectedHeaderItem() === "#completed" && getCompletedTodos().length > 0) && (
        <DeleteButton onClick={() => deleteCompletedTodos()}>
          <span className="material-icons">delete_outline</span>
          delete all
        </DeleteButton>
      )}
    </Container>
  );
}