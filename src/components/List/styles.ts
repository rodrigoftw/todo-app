import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  max-width: 608px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;

  @media(min-width: 320px) and (max-width: 620px) {
    padding: 0 2rem;
  } 
`;

export const CreateTodoContainer = styled.div`
  height: 3.5rem;
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin: 1.125rem 0 2rem;

  input {
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 17px;
    color: var(--gray);
    height: 100%;
    max-width: 476px;
    flex-grow: 1;
    border: 1px solid var(--gray2);
    border-radius: 0.75rem;
    padding: 0 0.75rem;
    
    @media(min-width: 320px) and (max-width: 620px) {
      height: 3.5rem;
    }
  }

  @media(min-width: 320px) and (max-width: 620px) {
    height: 8rem;
    flex-wrap: wrap;
  } 
`;

export const AddButton = styled.button`
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 17px;

  color: var(--button-text-color);
  background: var(--add-button-background);
  box-shadow: 0px 2px 6px rgba(127, 177, 243, 0.4);
  border-radius: 0.75rem;
  border: 0;
  height: 100%;
  width: 109px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  
  span {
    font-size: 16px;
  }

  @media(min-width: 320px) and (max-width: 620px) {
    height: 3.5rem;
    width: 100%;
  }
`;

interface TodoItemContainerProps {
  isCompleted?: boolean;
}

export const TodoItemContainer = styled.div<TodoItemContainerProps>`
  width: 100%;
  height: 2rem;
  margin: 0.2rem 0;
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  align-items: center;
  justify-content: flex-start;

  @media(min-width: 320px) and (max-width: 620px) {
    margin-top: ${props => props.isCompleted ? "1rem" : "0"};
  }
`;

export const DeleteButton = styled.button`
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  line-height: 15px;
  margin-top: 2rem;

  color: var(--button-text-color);
  background: var(--delete-button-background);
  border-radius: 0.25rem;
  border: 0;
  height: 40px;
  width: 124px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  align-self: flex-end;
  
  span {
    font-size: 16px;
  }

  @media(min-width: 320px) and (max-width: 620px) {
    width: 100%;
  }
`;