import styled from 'styled-components';

export const Container = styled.footer`
  font-family: "Montserrat", sans-serif;
  font-style: normal;
  font-size: 14px;
  line-height: 17px;
  color: #A9A9A9;

  height: 42px;
  width: auto;

  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  text-align: center;
`;

export const FooterTitle = styled.span`
  font-weight: 500;
`;

export const FooterLink = styled.a`
  font-weight: 700;
  margin: 0 4px;
  text-decoration: underline;
`;
