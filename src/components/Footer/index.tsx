import { Container, FooterTitle, FooterLink } from './styles';

export function Footer() {
  return (
    <Container>
      <FooterTitle>
        created by
        <FooterLink
          href="https://linktr.ee/rodrigodev"
          target="_blank"
          rel="noopener noreferrer"
        >
          Rodrigo Andrade
        </FooterLink>
         - devChallenges.io
      </FooterTitle>
    </Container>
  );
}