import { useHeader } from "../../hooks/useHeader";
import { Container, Title, NavMenu } from "./styles";

interface HeaderProps {
  href: string,
  className: string,
  title: string
}

export function Header() {
  const { menuOptions, selectHeaderItem } = useHeader();

  const handleSelectedOption = (href: string) => {
    selectHeaderItem(href);
  }
  return (
    <Container>
      <Title>#todo</Title>
      <NavMenu>
        {menuOptions.map((option: HeaderProps) => (
          <a
            key={option.href}
            href={option.href}
            className={option.className}
            onClick={() => handleSelectedOption(option.href)}
          >
            {option.title}
          </a>
        ))}
      </NavMenu>
    </Container>
  );
}