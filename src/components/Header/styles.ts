import styled from 'styled-components';

export const Container = styled.header`
  max-width: 608px;
  margin: 0 auto;
  text-align: center;
  padding: 2rem 12rem 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  border-bottom: 1px solid var(--gray2);

  @media(min-width: 320px) and (max-width: 620px) {
    width: 100%;
    max-width: 100%;
    padding: 2rem 2rem 0;
  } 
`;

export const Title = styled.h1`
  font-family: 'Raleway', sans-serif;
  font-weight: bold;
  font-size: 36px;
  line-height: 42px;
  text-align: center;
  letter-spacing: -0.045em;
  padding-bottom: 3.5rem;
`;

export const NavMenu = styled.nav`
  width: 608px;
  margin-top: auto;
  height: 3.5rem;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;

  @media(min-width: 320px) and (max-width: 620px) {
    width: 100%;
    display: grid;
    grid-template-columns: 85px 85px 85px;
    grid-template-rows: 3.5rem;
    column-gap: 0;
    row-gap: 0;
  } 

  a {
    display: inline-block;
    position: relative;
    padding: 0 0.5rem;
    height: 3.5rem;
    width: calc(100% /3);
    line-height: 3.5rem;
    color: var(--black);
    font-weight: 600;

    transition: color 0.2s;

    @media(min-width: 320px) and (max-width: 620px) {
      width: 100%;
      padding: 0;
    } 

    &:hover {
      color: var(--blue);
    }

    &.active::after {
      content: '';
      height: 3px;
      border-radius: 3px 3px 0 0;
      width: 50%;
      position: absolute;
      bottom: 0px;
      left: 25%;
      right: 25%;
      background: var(--blue);

      @media(min-width: 320px) and (max-width: 620px) {
        left: 0;
        right: 0;
        width: 85px;
      }
    }
  }
`;
