import {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState
} from "react";
import { v4 as uuidv4 } from 'uuid';

interface TodoProps {
  id: string;
  title: string;
  completed: boolean;
}

interface TodosProviderProps {
  children: ReactNode;
}

interface TodosContextData {
  todos: TodoProps[];
  getActiveTodos: () => TodoProps[];
  getCompletedTodos: () => TodoProps[];
  createTodo: (title: string) => void;
  updateTodoStatus: (id: string) => void;
  deleteTodo: (id: string) => void;
  deleteCompletedTodos: () => void;
}

const TodosContext = createContext<TodosContextData>(
  {} as TodosContextData
);

export function TodosProvider({ children }: TodosProviderProps) {
  const [todos, setTodos] = useState<TodoProps[]>(() => {
    const storagedTodos = localStorage.getItem('@ToDo:todos');

    if (storagedTodos) {
      return JSON.parse(storagedTodos);
    }

    return [];
  });

  useEffect(() => {
    setTodos(todos);
    localStorage.setItem('@ToDo:todos', JSON.stringify(todos));
  }, [todos]);

  const createTodo = (title: string) => {
    const newTodo = {
      id: uuidv4(),
      title,
      completed: false
    };

    setTodos([...todos, newTodo]);

    localStorage.setItem('@ToDo:todos', JSON.stringify([...todos, newTodo]));
  }

  const getActiveTodos = () => {
    return todos.filter(todo => todo.completed === false);
  }

  const getCompletedTodos = () => {
    return todos.filter(todo => todo.completed === true);
  }

  const updateTodoStatus = (id: string) => {
    setTodos(
      todos.map(todo => {
        if (todo.id === id) {
          return { ...todo, completed: !todo.completed };
        } else {
          return todo;
        }
      })
    );
    localStorage.setItem('@ToDo:todos', JSON.stringify(todos));
  }

  const deleteTodo = (id: string) => {
    const updatedTodoList = setTodos(todos.filter(todo => todo.id !== id));
    localStorage.setItem('@ToDo:todos', JSON.stringify(updatedTodoList));
  }

  const deleteCompletedTodos = () => {
    const noCompletedTodos = setTodos(todos.filter(todos => todos.completed === false));
    localStorage.setItem('@ToDo:todos', JSON.stringify(noCompletedTodos));
  }

  return (
    <TodosContext.Provider value={{
      todos,
      getActiveTodos,
      getCompletedTodos,
      createTodo,
      updateTodoStatus,
      deleteTodo,
      deleteCompletedTodos
    }}>
      {children}
    </TodosContext.Provider>
  );
};

export function useTodos() {
  const context = useContext(TodosContext);

  return context;
}