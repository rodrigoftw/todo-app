import {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState
} from "react";

interface HeaderProps {
  href: string,
  className: string,
  title: string
}

interface TodosProviderProps {
  children: ReactNode;
}

interface HeaderContextData {
  menuOptions: HeaderProps[];
  selectHeaderItem: (href: string) => void;
  getSelectedHeaderItem: () => string | "#all" | "#active" | "#completed";
}

const HeaderContext = createContext<HeaderContextData>(
  {} as HeaderContextData
);

export function HeaderProvider({ children }: TodosProviderProps) {

  const [menuOptions, setMenuOptions] = useState<HeaderProps[]>([
    {
      href: "#all",
      className: "active",
      title: "All"
    },
    {
      href: "#active",
      className: "",
      title: "Active"
    },
    {
      href: "#completed",
      className: "",
      title: "Completed"
    }
  ]);

  useEffect(() => {
    setMenuOptions(menuOptions);
  }, [menuOptions]);

  function selectHeaderItem(href: string) {
    return setMenuOptions(
      menuOptions.map(item => {
        if (item.href === href) {
          return { ...item, className: "active" };
        } else {
          return { ...item, className: "" };
        }
      })
    );
  }

  function getSelectedHeaderItem() {
    const selected =
      menuOptions.find((item: HeaderProps) => item.className === "active");

    return selected ? selected.href : "#all";
  }

  return (
    <HeaderContext.Provider value={{
      menuOptions,
      selectHeaderItem,
      getSelectedHeaderItem
    }}>
      {children}
    </HeaderContext.Provider>
  );
};

export function useHeader() {
  const context = useContext(HeaderContext);

  return context;
}