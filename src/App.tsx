import { Header } from "./components/Header";
import { List } from "./components/List";
import { Footer } from "./components/Footer";
import { GlobalStyle } from "./styles/global";

import { HeaderProvider } from "./hooks/useHeader";
import { TodosProvider } from "./hooks/useTodos";

function App() {
  return (
    <HeaderProvider>
      <TodosProvider>
        <Header />
        <List />
        <Footer />
        <GlobalStyle />
      </TodosProvider>
    </HeaderProvider>
  );
}

export default App;
