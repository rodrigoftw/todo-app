import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  :root {
    --white: #FFFFFF;
    --black: #333333;
    --black2: #000000;
    --gray: #828282;
    --gray2: #BDBDBD;
    --gray3: #A9A9A9;
    --blue: #2F80ED;
    --red: #EB5757;
    
    --text-title: var(--black);
    --tab-text-title: var(--black);
    --item-text-title: var(--black2);
    
    --background: var(--white);
    --button-text-color: var(--white);
    --add-button-background: var(--blue);
    --delete-button-background: var(--red);
  }

  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  html {
    @media(max-width: 1080px) {
      font-size: 93.75%;
    }
    
    @media(max-width: 720px) {
      font-size: 86.5%;
    }
  }

  body {
    background: var(--background);
    -webkit-font-smoothing: antialiased;
    font-family: 'Montserrat', sans-serif;
    font-weight: 500;
    height: 100vh;
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: stretch;
  }

  #root {
    height: 100%;
  }

  button {
    font-family: 'Montserrat', sans-serif;
    font-weight: 600;
  }
  
  input {
    font-family: 'Montserrat', sans-serif;
    font-weight: 400;
  }
  
  /* h1, h2, h3, h4, h5, h6, strong {
    font-family: 'Raleway', sans-serif;
    font-weight: 700;
  } */

  button {
    cursor: pointer;
  }
  
  a {
    color: inherit;
    text-decoration: none;
  }
`;
